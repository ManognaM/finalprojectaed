/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type,String name){
        Organization organization = null;
        if (type.getValue().equals(Type.Hospital.getValue())){
            organization = new HospitalOrganization();
            organization.setOrg_name(name);
            organizationList.add(organization);
            
        }
        else if (type.getValue().equals(Type.Lab.getValue())){
            organization = new LabOrganization();
            organization.setOrg_name(name);
            organizationList.add(organization);
            
            
        }
        
        else if (type.getValue().equals(Type.Pharmacy.getValue())){
            organization = new PharmacyOrganization();
            organization.setOrg_name(name);
            organizationList.add(organization);
            
            
        }
        return organization;
    }
}
