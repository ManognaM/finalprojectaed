/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Hospital;

import java.util.ArrayList;

/**
 *
 * @author Vaishali Tripathi
 */
public class HospitalDirectory {
    
    
    private ArrayList<Hospital> hospitalList;

    public HospitalDirectory() {
        hospitalList= new ArrayList();
    }

    public ArrayList<Hospital> getPharmacyList() {
        return hospitalList;
    }
    
    public Hospital createPharmacy(String name){
        Hospital pharm = new Hospital();
        pharm.setHospitalName(name);
        hospitalList.add(pharm);
        return pharm;
    }
}
